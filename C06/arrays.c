#include <stdio.h>

// to compile: gcc -o arrays.out arrays.c
// to run: ./arrays.out
int main(int argc, char **argv){

  int v[10];
  int n;

  for(n=0;n<10;n=n+1){
    v[n] = n;
  }

  for(n=0;n<10;n=n+1){
    // if n is even
    if((n % 2)==0)
      printf("v[%d] = %d\n",
	     n, v[n]);
  }
  return 0;
}
