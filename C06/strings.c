#include <stdio.h>

// to compile: gcc -o strings.out strings.c
// to run: ./strings.out

int main(int argc, char **argv){

  char name[5] = "fooo";

  printf("name %s \n", name);
  
  name[1] = 'l';

  printf("name %s \n", name);

  name[4] = 'g';
  name[5] = 'g';
  name[6] = 'g';

  printf("name %s \n", name);
  
  return 0;

}
