
#include <stdio.h>
#include <stdlib.h>

// to compile: gcc -o isInteger.out isInteger
// to run:
// ./isInteger.out bob
// ./isInteger.out 12345
// ./isInteger.out 123bob

int isInteger(char *s){

  int i = 0;

  // is this a null string ?
  if(s[0]='\0')
    return 0;
  
  while(s[i]!='\0'){

    if(s[i]<48) return 0;
    if(s[i]>57) return 0;

    i=i+1;
  }
  
  return 1;
  
}


int main(int argc, char **argv){

  int test = isInteger(argv[1]);

  printf("atoi(%s) = %d\n",
	 argv[1], atoi(argv[1]));
  
  if(test==0){
    printf("%s is not an integer\n",
	   argv[1]);
  }
  else{
    printf("%s is an integer\n",
	   argv[1]);

  }

  return 0;
}
