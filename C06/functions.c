#include <stdio.h>

// to compile: gcc -o functions.out functions.c
// to run: ./functions.out

void someFunction(int n){

  n = 4;

  return;

}

void someOtherFunction(int *pt_n){

  (*pt_n) = 4;

  return;

}

int main(int argc, char **argv){

  int n = 3;

  someOtherFunction(&n);

  printf("n=%d\n", n);

}
