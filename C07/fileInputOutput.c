
#include <stdlib.h>

#include <stdio.h>
// gcc -o fileInputOutput.out fileInputOutput.c
// run: ./fileInputOutput data.dat

int countLines(FILE *fp){

  char buf[BUFSIZ];
  int numberLines = 0;

  // get a label for the current position in the file
  long int start = ftell(fp);
  
  while(fgets(buf, BUFSIZ, fp)){
    ++numberLines;
  }

  // rewind back to the original start
  fseek(fp, start, SEEK_SET);

  return numberLines;
}

int main(int argc, char **argv){

  // "open" the file to read
  FILE *fp = fopen(argv[1], "r");

  // count number of lines in file
  int numberLines = countLines(fp);

  printf("numberLines=%d\n", numberLines);

  // create a data array by dynamic memory allocation
  float *v = (float*) malloc(numberLines*sizeof(float));
  
  // create a buffer on the stack
  char buf[BUFSIZ];

  int n;
  for(n=0;n<numberLines;++n){
    // read a line of the file
    fgets(buf, BUFSIZ, fp);
    int res = sscanf(buf, "%f", v+n);
    if(res==-1){
      break;
    }
  }

  if(n!=numberLines)
    numberLines = n;
  
  // "close" the file cos we are done
  fclose(fp);

  // loop through and find the minimum value in the array
  float minEntry = v[0];

  for(int i=1;i<numberLines;i++){
    if(v[i]<minEntry){
      minEntry = v[i];
      printf("minEntry = %f\n", minEntry);
    }
  }

  printf("minEntry = %f\n", minEntry);
  
}
