#!/usr/bin/env python3

import numpy as np
import time
import sys

N = int(float(sys.argv[1]))
H = 0

tic = time.time()

for i in range(N):
    x = np.random.rand()
    y = np.random.rand()

    if(x*x+y*y<1):
        H = H+1

toc = time.time()

estpi = 4*H/N

print('N=', N)
print('estpi=', estpi)
print('elapsed=', toc-tic, 's')


        
