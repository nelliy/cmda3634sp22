#!/usr/bin/env python3

import sys
import numpy.random as rnd
import time

count = 0
N = int(float(sys.argv[1]))

start = time.time()

for i in range(N):
    x = rnd.rand()
    y = rnd.rand()

    if(x*x+y*y<1):
        count = count+1

end = time.time()
        
estpi = count*4/N

print("N = ", N)
print("pi ~ ", estpi)
print("elapsed ~ ", end-start, "s")

        
