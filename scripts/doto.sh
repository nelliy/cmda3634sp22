#!/bin/bash
# Pass a verb and a file with student usernames (and any additional arguments)

if [ -z ${saynames+x} ] ; then
    # saynames is NOT set
    saynames=0
fi

if [ $# -lt 1 ] ; then
    echo "Use one of the following as your first argument:"
    echo "  distribute"
    echo "  clone"
    echo "  pull"
    echo "  push"
    echo "  reset_pull"
    echo "  update_push"
    echo "  checkout_time"
    echo "  run_test"
    echo "  inline"
    exit 1
fi

if [ $1 = "distribute" ] ; then
    # Copy a file into student repos
    if [ $# -ne 4 ] ; then
        echo "distribute mode needs exactly 4 arguments:"
        echo "1: distribute"
        echo "2: student list filename"
        echo "3: file to copy to all student directories"
        echo "4: destination directory relative to student repo root"
        exit 1
    fi
    while read L; do
        echo
        if [ $saynames -eq 1 ] ; then
            echo "================"
            echo "$L"
            echo "================"
        else
            echo
            echo "================"
            echo "$L" | sed 's/\(.\).*\(.\)/\1----\2/'
            echo "================"
        fi
        studentRoot=~/cmda3634/$L
        dest=$studentRoot/$4
        mkdir -p $dest
        cp $3 $dest
    done < $2
elif [ $1 = "update_push" ] ; then
    # pull student repos and then add and commit my working tree
    if [ $# -ne 2 -a $# -ne 3 ] ; then
        echo "update_push mode needs 2 or 3 arguments:"
        echo "1: update_push"
        echo "2: student list filename"
        echo "3: (optionally) a commit message"
        exit 1
    fi
    if [ $# -eq 2 ] ; then
        message="Instructor file distribution"
    else
        message="$3"
    fi

    while read L; do
        echo
        if [ $saynames -eq 1 ] ; then
            echo "================"
            echo "$L"
            echo "================"
        else
            echo
            echo "================"
            echo "$L" | sed 's/\(.\).*\(.\)/\1----\2/'
            echo "================"
        fi
        studentRoot=~/cmda3634/$L
        git -C $studentRoot pull
        # The new default branch name is "main" because woke
        git -C $studentRoot checkout main
        if [ $? -ne 0 ] ; then
            git -C $studentRoot checkout master
        fi
        git -C $studentRoot add .
        git -C $studentRoot commit -m "$message"
        git -C $studentRoot push
    done < $2
elif [ $1 = "reset_pull" ] ; then
    if [ $# -ne 2 ]; then
        echo "reset_pull mode takes two arguments:"
        echo "1: reset_pull"
        echo "2: student list filename"
        exit 1
    fi

    while read L; do
        echo
        if [ $saynames -eq 1 ] ; then
            echo "================"
            echo "$L"
            echo "================"
        else
            echo
            echo "================"
            echo "$L" | sed 's/\(.\).*\(.\)/\1----\2/'
            echo "================"
        fi
        studentRoot=~/cmda3634/$L
        # Put me back on the master branch if I got off it somehow
        git -C $studentRoot checkout main
        if [ $? -ne 0 ] ; then
            git -C $studentRoot checkout master
        fi
        # Remove all untracked files (besides ignored ones)
        git -C $studentRoot clean -f
        # Remove any local changes that I may have made
        git -C $studentRoot reset --hard
        # Pull their version
        git -C $studentRoot pull
    done < $2
elif [ $1 = "pull" ] ; then
    if [ $# -ne 2 ]; then
        echo "reset_pull mode takes two arguments:"
        echo "1: pull"
        echo "2: student list filename"
        exit 1
    fi

    while read L; do
        echo
        if [ $saynames -eq 1 ] ; then
            echo "================"
            echo "$L"
            echo "================"
        else
            echo
            echo "================"
            echo "$L" | sed 's/\(.\).*\(.\)/\1----\2/'
            echo "================"
        fi
        studentRoot=~/cmda3634/$L
        git -C $studentRoot pull
    done < $2
elif [ $1 = "clone" ] ; then
    if [ $# -ne 2 ]; then
        echo "clone mode takes two arguments:"
        echo "1: clone"
        echo "2: student list filename"
        exit 1;
    fi
    while read L; do
        echo
        if [ $saynames -eq 1 ] ; then
            echo "================"
            echo "$L"
            echo "================"
        else
            echo
            echo "================"
            echo "$L" | sed 's/\(.\).*\(.\)/\1----\2/'
            echo "================"
        fi
        studentRoot=~/cmda3634/$L
        # Sigh. Just check for the two who didn't follow directions
        if [ $L = xiaoyang19 ] ; then
            git clone git@code.vt.edu:$L/cmda_3634.git $studentRoot
        elif [ $L = jaredbyers ] ; then
            git clone git@code.vt.edu:$L/cmda3634j.git $studentRoot
        else
            git clone git@code.vt.edu:$L/cmda3634.git $studentRoot
        fi
    done < $2
elif [ $1 = "run_test" ] ; then
    if [ $# -ne 4 ]; then
        echo "run_test mode takes four arguments:"
        echo "1: run_test"
        echo "2: student list filename"
        echo "3: directory relative to repo root where test should be run"
        echo "4: full path of test script"
        exit 1;
    fi
    while read L; do
        echo
        if [ $saynames -eq 1 ] ; then
            echo "================"
            echo "$L"
            echo "================"
        else
            echo
            echo "================"
            echo "$L" | sed 's/\(.\).*\(.\)/\1----\2/'
            echo "================"
        fi
        studentRoot=~/cmda3634/$L
        mkdir -p $studentRoot/$3
        cd $studentRoot/$3
        $4
    done < $2
elif [ $1 = "checkout_time" ] ; then
    echo "This mode does NOT pull"
    if [ $# -ne 5 ]; then
        echo "checkout_time mode takes four arguments:"
        echo "1: checkout_time"
        echo "2: student list filename"
        echo "3: date (e.g. 2022-05-25)"
        echo "4: time (e.g. 01:00:00)"
        echo "   timezone -0500 is assumed"
        exit 1
    fi
    while read L; do
        echo
        if [ $saynames -eq 1 ] ; then
            echo "================"
            echo "$L"
            echo "================"
        else
            echo
            echo "================"
            echo "$L" | sed 's/\(.\).*\(.\)/\1----\2/'
            echo "================"
        fi
        studentRoot=~/cmda3634/$L
        #git -C $studentRoot checkout `git -C $studentRoot rev-list -n 1 --first-parent --before="$3 $4 $5" master`
        git -C $studentRoot checkout main
        if [ $? -ne 0 ] ; then
            git -C $studentRoot checkout `git -C $studentRoot rev-list -n 1 --before="$3 $4 -0500" master`
        else
            git -C $studentRoot checkout `git -C $studentRoot rev-list -n 1 --before="$3 $4 -0500" main`
        fi
    done < $2
elif [ $1 = "inline" ] ; then
    if [ $# -lt 3 ]; then
        echo "inline mode takes three arguments:"
        echo "1: inline"
        echo "2: student list filename"
        echo "3: command to run in student repo root directory"
        echo "   (may have spaces, does not need to be in quotes)"
        exit 1;
    fi
    while read L; do
        echo
        if [ $saynames -eq 1 ] ; then
            echo "================"
            echo "$L"
            echo "================"
        else
            echo
            echo "================"
            echo "$L" | sed 's/\(.\).*\(.\)/\1----\2/'
            echo "================"
        fi
        studentRoot=~/cmda3634/$L
        cd $studentRoot
        ${@:3}
    done < $2
else
    echo "Unknown mode $1"
    exit 1
fi

