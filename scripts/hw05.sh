#!/bin/bash

gcc -o stdev.out stdev.c -lm
echo "7 8 WITHOUT sample correction"
./stdev.out $1/biased.dat
echo "7 8 WITH sample correction"
./stdev.out $1/unbiased.dat

