
#include <stdio.h>
#include <stdlib.h>

int main(int argc,char **argv){

  if(argc!=2){
    printf("usage: %s n\n", argv[0]);
    exit(-1);
  }
  
  int n = atoi(argv[1]);

  printf("n=%d\n", n);

  return 0;

}
