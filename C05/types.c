#include <stdio.h>

int main(int argc, char **argv){
  
  int i = 3565;
  int j = 666;
  int k = -34556;

  int l = i + k;
  int m = i/j;
  printf("i=%d, j=%d, m=%d\n", i, j, m);

  float a = 1.5;
  float b = 1.49999999;
  printf("a=%e, b=%e\n, a-b=%e", a, b, a-b);

  float *pt_a = &a;

  pt_a[0] = 1.2;
  *pt_a = 1.2;

  int *pt_i = &i;
  int offset = 0;
  printf("Starting snooping\n");
  while(offset<100){
    printf("%d \n", pt_i[offset]);
    offset = offset+1;
  }
  
  return 0;

}
