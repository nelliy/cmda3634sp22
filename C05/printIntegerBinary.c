#include <stdio.h>
#include <stdlib.h>

// to compile: gcc -o printIntegerBinary printIntegerBinary.c
// to run: ./printIntegerBinary 7

int main(int argc, char **argv){

  int Nbits = sizeof(int)*8;
  unsigned int mask = 1;

  // print binary
  int a = atoi(argv[1]);

  unsigned int lla = *((unsigned int*)(&a));

  printf("INT32: a=%d => binary: ", a);
  for(int b=0;b<Nbits;++b){
    unsigned int maskb = mask << (Nbits-1-b);

    if(b==1) printf("|");

    if(lla&maskb)
      printf("1");
    else
      printf("0");
  }
  printf("\n");

  return 0;
}
