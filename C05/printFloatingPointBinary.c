#include <stdio.h>
#include <stdlib.h>

#define uint64_t unsigned long long int

// to compile: gcc -o printFloatingPointBinary printFloatingPointBinary.c
// to run: ./printFloatingPointBinary 7

int main(int argc, char **argv){

  int Nbits = sizeof(double)*8;
  uint64_t mask = 1;

  // print binar
  
  double a = atof(argv[1]);

  uint64_t lla = *((uint64_t*)(&a));

  printf("FP64: a=%g => binary: ", a);
  for(int b=0;b<Nbits;++b){
    uint64_t maskb = mask << (Nbits-1-b);

    if(b==1 | b==12) printf("|");

    if(lla&maskb)
      printf("1");
    else
      printf("0");
  }
  printf("\n");

  return 0;
}
