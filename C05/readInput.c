#include <stdio.h>
#include <stdlib.h>

// read a number from the command line arguments

int main(int argc, char **argv){

  int n; // not initialized

  printf("function is named %s\n", argv[0]);

  // read command line argument
  n = atoi(argv[1]); 

  printf("n=%d\n", n);

  return 0;
}
